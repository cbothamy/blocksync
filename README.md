# Blocksync

## A block-level synchronization utility

## What is blocksync.py?

Blocksync.py is an utility which can synchronize content of two large (block) files
between two different machines.
Blocksync is useful in the context of very large files (>10GB)
where utilities (e.g. rsync) not working on a block-level 
take too much time even if supplied with correct set of parameters.

I use blocksync to backup my raw hard-drive content (160GB) 
but you can use it for other block devices as well (e.g. crypted volumes/files)

## Installation

Obviously, you need a Python. Apart from that, blocksync.py script must be installed 
on both local and remote machine and it must be accessible via ssh on the remote machine.

## Usage

  sudo blocksync.py --direction=push /dev/sda login@remote.com /backup/disk.raw [--blocksize=65536]

If you want to synchronize two files on the same machine, just use login@localhost as a remote.

Note that Blocksync will complain if the destination file is not of the same size. 

## Algorithm
Blocksync tries to transfer only the differences in the files 
(this is cruical for raw harddrive backups over the network). 
The algorithm takes hashes of blocks (of size --blocksize) 
and sends them to the remote.
If the hash does not match, Blocksync will transfer the whole block.

Apart from the blocks, Blocksync uses a notion of superblocks, 
which is a collection of several blocks. 
Blocksync first compares hashes of superblocks and only if they are different, 
it proceeds with comparing hashes of blocks. This helps with

* limiting the number of transmitted bytes when there is no content difference
* improving the performance - current version of Blocksync works in "synchronous" mode
  which means that blocks needs to compared and sent in an serial fashion.
  This in turns limits the Blocksync performance on connections
  with bigger latencies (for each block we need to wait the round-trip time)

